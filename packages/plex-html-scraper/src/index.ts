import {
  DataRinPlexEpisodeContext,
  DataRinPlexEpisodeScrapFunction,
  PlexEpisodeScrapResult
} from "@datarin/plex-episode-scraper";

import scrapDom, {
  DomScrapOptions,
  DomScrapResult
} from "@datarin/dom-scraper";

export interface DataRinPlexHtmlScrapOptions extends DomScrapOptions {
  transform: (
    result: DomScrapResult,
    context: DataRinPlexEpisodeContext,
    options: DataRinPlexHtmlScrapOptions
  ) => PlexEpisodeScrapResult;
}

const scrapHtml: DataRinPlexEpisodeScrapFunction<DataRinPlexHtmlScrapOptions> = async (
  context,
  options
) => {
  const { transform, ...domOptions } = options;
  const domResult = await scrapDom(context, domOptions);
  return transform(domResult, context, options);
};

export default scrapHtml;
