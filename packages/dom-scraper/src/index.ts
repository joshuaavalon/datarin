import { DataRinContext, DataRinScrapFunction } from "@datarin/core";
import scrapWeb, { WebScrapOptions } from "@datarin/web-scraper";
import cheerio from "cheerio";

export interface DomScrapResult {
  url: string;
  dom: CheerioStatic;
}

export interface DomScrapOptions extends WebScrapOptions {}

const scrapDom: DataRinScrapFunction<
  DomScrapOptions,
  DataRinContext,
  DomScrapResult
> = async (context, options) => {
  const { url, res } = await scrapWeb(context, options);
  const dom = cheerio.load(res.data);
  return { url, dom };
};

export default scrapDom;
