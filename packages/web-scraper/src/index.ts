import { DataRinContext, DataRinScrapFunction } from "@datarin/core";
import axios, { AxiosResponse } from "axios";
import { render } from "mustache";
import lru from "tiny-lru";

export interface WebScrapResult {
  url: string;
  res: AxiosResponse<any>;
}

export interface WebScrapOptions {
  url: string;
  headers?: Record<string, string>;
  validateStatus?: (status: number) => boolean;
  cache?: boolean;
}

export const defaultHeaders = {
  "User-Agent":
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36"
};

const lruCache = lru<WebScrapResult>(1000);

const scrapWeb: DataRinScrapFunction<
  WebScrapOptions,
  DataRinContext,
  WebScrapResult
> = async (context, options) => {
  const {
    url,
    headers = { ...defaultHeaders },
    validateStatus = (status: number) => status === 200,
    cache = true
  } = options;
  const targetUrl = render(url, context);
  if (cache) {
    const result = lruCache.get(targetUrl);
    if (result) {
      return result;
    }
  }
  const res = await axios.get(targetUrl, {
    headers,
    validateStatus
  });
  const result = { url: targetUrl, res };
  if (cache) {
    lruCache.set(targetUrl, result);
  }
  return result;
};

export default scrapWeb;
