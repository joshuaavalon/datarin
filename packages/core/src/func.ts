import _ from "lodash";

import { DataRinContext, DataRinScrapFunction } from "./model";

export interface DataRinOptionPlugin<
  C extends DataRinContext = DataRinContext,
  T extends {} = {}
> {
  scraper: DataRinScrapFunction<C, T> | string;
  options?: Record<string, any>;
}

export interface DataRinOptions<
  C extends DataRinContext = DataRinContext,
  T extends {} = {}
> {
  plugins: DataRinOptionPlugin<C, T>[];
}

export const scrap = async <
  C extends DataRinContext = DataRinContext,
  T extends {} = {}
>(
  context: C,
  options: DataRinOptions<C, T>
): Promise<T[]> => {
  const { plugins } = options;
  const promises = plugins.map(async plugin => {
    const { scraper, options = {} } = plugin;
    const scrapFunc: DataRinScrapFunction<C, T> = _.isString(scraper)
      ? await import(scraper)
      : scraper;
    return scrapFunc(context, options);
  });
  return Promise.all(promises);
};
