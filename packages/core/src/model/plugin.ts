import { DataRinContext } from "./context";

export interface DataRinScrapFunction<
  O extends {},
  C extends DataRinContext = DataRinContext,
  T extends {} = {}
> {
  (context: C, options: O): Promise<T>;
}
