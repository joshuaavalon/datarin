import { DataRinContext } from "./context";
export interface DataRinScrapPlugin<T extends {}, C extends DataRinContext> {
    isScrapTypeSupported: (scrapType: string) => boolean;
    scrap(context: C): T;
}
