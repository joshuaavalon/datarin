export interface DataRinContext<C extends DataRinUserConfiguration = DataRinUserConfiguration> {
    config: C;
}
export interface DataRinUserConfiguration {
    scrapType: string;
}
