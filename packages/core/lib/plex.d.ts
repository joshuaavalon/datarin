import { DataRinContext, DataRinUserConfiguration } from "./context";
export interface DataRinUserPlexEpisodeConfiguration extends DataRinUserConfiguration {
    scrapType: "plex";
}
export interface DataRinPlexEpisodeContext extends DataRinContext<DataRinUserPlexEpisodeConfiguration> {
}
