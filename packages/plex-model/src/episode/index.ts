import _ from "lodash";

import { Assert, assert, Validate, validate } from "../validate";
import schema from "./schema.json";

export interface Episode {
  title: string[];
  aired: string | null;
  contentRating: string;
  summary: string | null;
  directors: string[];
  writers: string[];
  rating: number | null;
}

const schemas = { episode: schema };

export const isEpisode: Validate<Episode> = validate("episode", schemas);

export const assertIsEpisode: Assert<Episode> = assert("episode", schemas);

export const pickEpisode = (value: Episode): Episode =>
  _.pick(value, [
    "title",
    "aired",
    "contentRating",
    "summary",
    "directors",
    "writers",
    "rating"
  ]);
