import _ from "lodash";

import { Assert, assert, Validate, validate } from "../validate";
import schema from "./schema.json";
import { Actor } from "../actor";
import actorSchema from "../actor/schema.json";

export interface Movie {
  title: string;
  sortTitle: string;
  originalTitle: string[];
  contentRating: string;
  tagline: string[];
  studio: string[];
  aired: string | null;
  summary: string | null;
  rating: number | null;
  genres: string[];
  collections: string[];
  actors: Actor[];
  directors: string[];
  writers: string[];
}

const schemas = { movie: schema, actor: actorSchema };
export const isMovie: Validate<Movie> = validate("movie", schemas);

export const assertIsMovie: Assert<Movie> = assert("movie", schemas);

export const pickMovie = (value: Movie): Movie =>
  _.pick(value, [
    "title",
    "sortTitle",
    "originalTitle",
    "contentRating",
    "tagline",
    "studio",
    "aired",
    "summary",
    "rating",
    "genres",
    "collections",
    "actors",
    "directors",
    "writers"
  ]);
