import _ from "lodash";

import { Assert, assert, Validate, validate } from "../validate";
import schema from "./schema.json";
import { Actor } from "../actor";
import actorSchema from "../actor/schema.json";

export interface Show {
  title: string;
  sortTitle: string;
  originalTitle: string[];
  contentRating: string;
  tagline: string[];
  studio: string[];
  aired: string | null;
  summary: string | null;
  rating: number | null;
  genres: string[];
  collections: string[];
  actors: Actor[];
  seasonSummary: { [key: number]: string };
}

const schemas = { show: schema, actor: actorSchema };
export const isShow: Validate<Show> = validate("show", schemas);

export const assertIsShow: Assert<Show> = assert("show", schemas);

export const pickShow = (value: Show): Show =>
  _.pick(value, [
    "title",
    "sortTitle",
    "originalTitle",
    "contentRating",
    "tagline",
    "studio",
    "aired",
    "summary",
    "rating",
    "genres",
    "collections",
    "actors",
    "seasonSummary"
  ]);
