import _ from "lodash";

import { Assert, assert, Validate, validate } from "../validate";
import schema from "./schema.json";

export interface Actor {
  name: string;
  role: string;
  photo: string | null | undefined;
}

const schemas = { actor: schema };

export const isActor: Validate<Actor> = validate("actor", schemas);

export const assertIsActor: Assert<Actor> = assert("actor", schemas);

export const pickActor = (value: Actor): Actor =>
  _.pick(value, ["name", "role", "photo"]);
