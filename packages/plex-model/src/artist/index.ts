import _ from "lodash";

import { Assert, assert, Validate, validate } from "../validate";
import schema from "./schema.json";

export interface Artist {
  sortTitle: string;
  genres: string[];
  collections: string[];
  summary: string | null;
  similar: string[];
}

const schemas = { artist: schema };

export const isArtist: Validate<Artist> = validate("artist", schemas);

export const assertIsArtist: Assert<Artist> = assert("artist", schemas);

export const pickArtist = (value: Artist): Artist =>
  _.pick(value, ["sortTitle", "genres", "collections", "summary", "similar"]);
