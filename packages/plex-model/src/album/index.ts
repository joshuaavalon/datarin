import _ from "lodash";

import { Assert, assert, Validate, validate } from "../validate";
import schema from "./schema.json";

export interface Album {
  aired: string;
  collections: string[];
  genres: string[];
  summary: string | null;
}

const schemas = { album: schema };

export const isAlbum: Validate<Album> = validate("album", schemas);

export const assertIsAlbum: Assert<Album> = assert("album", schemas);

export const pickAlbum = (value: Album): Album =>
  _.pick(value, ["aired", "collections", "genres", "summary"]);
