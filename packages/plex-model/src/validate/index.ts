import Ajv, { ErrorObject } from "ajv";
import _ from "lodash";

import schema from "./schema.json";

type ValidationResult = {
  valid: boolean;
  errors: ErrorObject[];
};

function validateSchema<
  T extends Record<string, any>,
  U extends keyof T & string
>(
  data: any,
  name: U,
  schemas: Record<string, any>,
  options: Ajv.Options = { allErrors: true, format: "full" }
): ValidationResult {
  const ajv = new Ajv(options);
  ajv.addSchema(schema);
  _.forEach(schemas, (value, key) => {
    ajv.addSchema(value, key);
  });
  const validate = ajv.getSchema(name);
  if (!validate) {
    return {
      valid: false,
      errors: []
    };
  }
  const valid = validate(data) as boolean;
  return {
    valid,
    errors: validate.errors || []
  };
}

export class ValidationError extends Error {
  errors: ErrorObject[];
  public constructor(errors: ErrorObject[]) {
    const errorMessage = errors
      .map(err => err.message)
      .filter(msg => !!msg)
      .join(", ");
    super(errorMessage);
    this.errors = errors;
  }
}

export type Validate<T> = (value: any) => value is T;
export type Assert<T> = (value: any) => asserts value is T;

export function validate<
  T extends Record<string, any>,
  U extends keyof T & string,
  V
>(name: U, schemas: T): Validate<V> {
  return (value): value is V => {
    const result = validateSchema(value, name, schemas);
    return result.valid;
  };
}

export function assert<
  T extends Record<string, any>,
  U extends keyof T & string,
  V
>(name: U, schemas: T): Assert<V> {
  return (value): asserts value is V => {
    const result = validateSchema(value, name, schemas);
    if (!result.valid) {
      throw new ValidationError(result.errors);
    }
  };
}
