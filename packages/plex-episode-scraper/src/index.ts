import { DataRinContext, DataRinScrapFunction } from "@datarin/core";
import { Episode } from "@datarin/plex-model";

export interface DataRinPlexEpisodeContext extends DataRinContext {
  season: number;
  episode: number;
}

export interface PlexEpisodeScrapResult {
  episode: Episode;
  thumbnails: string[];
}

export interface DataRinPlexEpisodeScrapFunction<O extends {}>
  extends DataRinScrapFunction<
    O,
    DataRinPlexEpisodeContext,
    PlexEpisodeScrapResult
  > {}
